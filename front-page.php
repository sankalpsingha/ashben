<?php get_header(); ?>

<header class="mb30">
        <div class="container">
            <div class="row">
                <h1>We specialize in <br> <span>SOURCING and LOGISTICS</span></h1>
            </div>
            <div class="row">
                    <a href="#" class="header-button">Learn More</a>
            </div>
        </div>
    </header>

    <section class="mb30">
        <div class="container">
            <h2 class="text-center mb30">OUR SOLUTIONS</h2>
            <div class="row mb30">
                    <div class="solution-box col-md-4">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/Ashben Icons_pipes lined.png' ?>" alt="Pipes Image" class="solution-box__image">
                            <h3 class="solution-box__title">Pipes and tubes</h3>
                        </div>
                
                    <div class="solution-box col-md-4">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/Ashben Icons_Angles lined.png' ?>" alt="" class="solution-box__image">
                        <h3 class="solution-box__title">Angles & Bars</h3>
                    </div>
            
                    <div class="solution-box col-md-4">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/Ashben Icons_Fittings lined.png' ?>" alt="" class="solution-box__image">
                        <h3 class="solution-box__title">Pipe Fittings</h3>
                    </div>
            </div>


            <div class="row">
                    <div class="solution-box col-md-4">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/Ashben Icons_Sheets lined.png' ?>" alt="" class="solution-box__image">
                            <h3 class="solution-box__title">Coils, Sheets & Plates</h3>
                        </div>
                
                    <div class="solution-box col-md-4">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/Ashben Icons_Valves lined.png' ?>" alt="" class="solution-box__image">
                        <h3 class="solution-box__title">Industrial Valves</h3>
                    </div>
            
                    <div class="solution-box col-md-4">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/Ashben Icons_Flanges lined.png' ?>" alt="" class="solution-box__image">
                        <h3 class="solution-box__title">Pipe Flanges</h3>
                    </div>
            </div>
        </div>
    </section>

    <section class="catalog">
                <img src="<?php echo get_template_directory_uri() . '/assets/images/document.png' ?>" alt="Pdf image" class="document-image"> 
                            <h2 class="">Read about all our products</h2>
                            <h3>Download our product catalogue and learn more about the product ranges suited to fit in all engineering applications.</h3>
                            
                    <a href="#" class="download-button">Download catalog</a>
    </section>

    <section class="contact">
            <div class="form-section">
                <?php 
                   echo do_shortcode('[ninja_form id=1]');
                ?>
            </div>
            <div class="text-section">
                <h2>We would love to hear <br> from you!</h2>
                <h3>You may contact us for <br> any queries from the <br> form, and our experts <br> will get back to you soon.</h3>
            </div>
           </section>


<?php get_footer(); ?>