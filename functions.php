<?php 

//Setup
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php.txt';
require_once get_template_directory() . '/includes/libs/class-tgm-plugin-activation.php';

// Includes
 
include( get_template_directory() . '/includes/front/enqueue.php' );
include( get_template_directory(). '/includes/setup.php');
include( get_template_directory(). '/includes/theme-customizer.php');
include( get_template_directory(). '/includes/widgets.php');
include ( get_template_directory() . '/includes/tgmpa_plugins.php');


// Hooks

add_action('wp_enqueue_scripts', 'ashben_enqueue');
add_action('after_setup_theme', 'ashben_setup_theme');
add_action('customize_register', 'ashben_customize_register');
add_action('widgets_init', 'ashben_widgets');

add_action( 'tgmpa_register', 'ashben_register_required_plugins' );
// Filters


// Shortcodes

