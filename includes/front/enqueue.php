<?php

function ashben_enqueue() {
    wp_register_style('ashben_google_fonts', 'https://fonts.googleapis.com/css?family=Montserrat:100,300,400,400i');
    wp_register_style('ashben_bootstrap', get_template_directory_uri() . '/assets/vendor/css/bootstrap.min.css');
    wp_register_style('ashben_style', get_template_directory_uri(). '/assets/css/style.css');

    // Calling the enqueue script
    wp_enqueue_style('ashben_google_fonts');
    wp_enqueue_style('ashben_bootstrap');
    wp_enqueue_style('ashben_style');

    wp_register_script('ashben_popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'), false, true);
    wp_register_script('ashben_bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), false, true);
    
    // wp_enqueue_script('jquery');
    wp_enqueue_script('ashben_popper');
    wp_enqueue_script('ashben_bootstrap');

}


function jquery_mumbo_jumbo()
{
    wp_dequeue_script('jquery');
    wp_dequeue_script('jquery-core');
    wp_dequeue_script('jquery-migrate');
    wp_enqueue_script('jquery', false, array(), false, true);
    wp_enqueue_script('jquery-core', false, array(), false, true);
    wp_enqueue_script('jquery-migrate', false, array(), false, true);
}
add_action('wp_enqueue_scripts', 'jquery_mumbo_jumbo');