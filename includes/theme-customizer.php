<?php

function ashben_customize_register($wp_customize) {
    $wp_customize->add_setting('ashben_copyright', array(
        'default' => 'Copyright 2019'
    ));

    $wp_customize->add_section('ashben_footer_section', array(
        'title' => __('Ashben Footer Info', 'ashben'),
        'priority' => 30,
    ));

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'ashben_copyright_input',
            array(
                'label'          => __( 'Copyright Text', 'ashben' ),
                'section'        => 'ashben_footer_section',
                'settings'       => 'ashben_copyright'
            )
        )
    );

}