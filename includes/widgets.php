<?php 

function ashben_widgets() {
    register_sidebar(array(
        'name' => __('Footer 1', 'ashben'),
        'id' => 'ashben_footer_one',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
    ));


    register_sidebar(array(
        'name' => __('Footer 2', 'ashben'),
        'id' => 'ashben_footer_two',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'name' => __('Footer 3', 'ashben'),
        'id' => 'ashben_footer_three',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
    ));


    register_sidebar(array(
        'name' => __('Footer 4', 'ashben'),
        'id' => 'ashben_footer_four',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
    ));

}