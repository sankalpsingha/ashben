<footer class="pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                <?php if( is_active_sidebar( 'ashben_footer_one' ) ) : ?>
                        <?php dynamic_sidebar( 'ashben_footer_one' ); ?>
                <?php endif; ?>
                </div>
                <div class="col-md-3">
                    <?php if( is_active_sidebar( 'ashben_footer_two' ) ) : ?>
                            <?php dynamic_sidebar( 'ashben_footer_two' ); ?>
                    <?php endif; ?>
                </div>
                <div class="col-md-3">
                    <?php if( is_active_sidebar( 'ashben_footer_three' ) ) : ?>
                            <?php dynamic_sidebar( 'ashben_footer_three' ); ?>
                    <?php endif; ?>
                </div>
                <div class="col-md-3">
                    <?php if( is_active_sidebar( 'ashben_footer_four' ) ) : ?>
                            <?php dynamic_sidebar( 'ashben_footer_four' ); ?>
                    <?php endif; ?>
                </div>
            </div>
            
        </div>

        <div class="row mt-5">
                <div class="col sub-footer">
                    <p class="text-center mt-3">
                        <?php 
                            if(get_theme_mod('ashben_copyright')) {
                                echo get_theme_mod('ashben_copyright');
                            }
                        ?>
                    </p>
                </div>
            </div>
        
    </footer>

    <?php wp_footer(); ?>
  
</body>
</html>